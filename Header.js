import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

const Header = () => {
  return (
    <View style={styles.kotak}>
      <Text>Header</Text>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  kotak: {
    height: 100,
    width: '100%',
    backgroundColor: 'yellowgreen',
  },
});
