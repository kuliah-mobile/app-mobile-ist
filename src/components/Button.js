import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';

const Button = ({label, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.kotak}>
      <Text style={styles.label}>{label}</Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  kotak: {
    margin: 5,
    backgroundColor: 'green',
    padding: 5,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    color: 'white',
  },
});
