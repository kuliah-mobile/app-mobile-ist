import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

const Header = () => {
  return (
    <View style={styles.kotak}>
      <Text>Nama Aplikasi</Text>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  kotak: {
    backgroundColor: 'green',
    padding: 10,
  },
});
