import React from 'react';
import {View, Text} from 'react-native';
import {Button, Card, Header} from '../components';

const Tulis = () => {
  return (
    <>
      <Text>Ini tulisan saya yang ganteng</Text>
    </>
  );
};

const About = () => {
  return (
    <View>
      <Header />
      <Text>Halo</Text>
      <Tulis />
      <Tulis />
      <Card />
      <Button label={'Simpan'} />
      <Button label={'Batal'} />
    </View>
  );
};

export default About;
