import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';
import {Button, Header} from '../components';

export default class Home extends Component {
  render() {
    return (
      <View>
        <Header />
        <Text>Home</Text>
        <Button
          label={'Ke About'}
          onPress={() => this.props.navigation.navigate('About')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({});
